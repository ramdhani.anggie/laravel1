<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Halaman Register</h1>
    <form action="/kirim" method="POST">
        @csrf

        <label> Nama Depan</label><br><br>
        <input type="text" name="nama"><br><br> 
        <label> Nama Belakang</label><br><br>
        <input type="text" name="namabelakang"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gn"> Male<br>
        <input type="radio" name="gn"> Female<br>
        <input type="radio" name="gn"> Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="nt">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
            <option value="4">Brunei</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="spoken"> Bahasa Indonesia<br>
        <input type="checkbox" name="spoken"> English<br>
        <input type="checkbox" name="spoken"> Other<br><br>

        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="kirim">
</body>
</html>